<?php
namespace App\Lib;

class Response
{
    public $response = false;
    public $message = 'Ocurrio un error inesperado';
    public $result = null;

    public function SetResponse($response, $m = ''){
        $this->response = $response;
        $this->message = $m;

        if(!$response && $m == '') $this->response = $this->message;
    }
}