<?php
use App\Model\UserModel;

//Grupo de rutas para empleados
$app->group('/employee',function(){

    //Obtener todos los Emepleados
    $this->get('',function ($req, $resp, $args) {
        
        $users = new UserModel();
        
        /*return $resp
            ->withHeader('Content-type','application/json')
            ->getBody()
            ->write(
                json_encode($users->GeAll())
            );*/
            return $resp->withJson($users->GeAll());
    });

    //Obtener un empleado por id
    $this->get('/{id}',function ($req, $resp, $args) {
        $user = new UserModel();
        
        return $resp->withJson($user->Get($args['id']));
    });

    //from-data
    //Insertar ó actualizar un empleado
    $this->post('',function($req, $resp, $args){
        $user = new UserModel();
        //var_dump($req->getParseBody());die();
        return $resp->withJson($user->InsertOrUpdate($req->getParsedBody()));
    });

    //Eliminar un empleado
    $this->delete('/[{id}]',function($req, $resp, $args){
        $user = new UserModel();
        return $resp->withJson($user->Delete($args['id']));
    });
});